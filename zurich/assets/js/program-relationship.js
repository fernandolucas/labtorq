(function () {
   let openRow = false;
   let init = function () {
      document.getElementById('container-scores').addEventListener('load', scores());
      $('.tabs-body').css({ height: $('.tabs-container-item-1').height() + 'px' });
      $('.tabs-header').on('click', tabsAnimated);
      $('.btn-arrow').on('click', toogleCategory);
   };

   let scores = function () {
      let margin = 0;
      let pos_current = 0;
      let time = 1000;
      const option = $('#container-scores').data('score');
      if (option <= 1 || option > 5) return;

      const pos_start = $('.score-frame-1').offset().left;
      const pos_end = $('.score-frame-5').offset().left + $('.score-frame-5').width() + 10 - pos_start;

      if (option > 1 && option < 5) {
         margin = 4;
         pos_current = $('.score-circle-' + option).offset().left;
      }

      let move = option < 5 ? (pos_current - pos_start + margin) + 'px' : pos_end;
      $('.score-current').css({ left: ($('.score-current').width() + 10) * -1 + 'px' });
      $('.score-frame-dynamic').css({ left: move, transition: 'all ' + time * option + 'ms ease-in-out' });
   }

   let tabsAnimated = function () {
      const clicked_name = 'tabs-header';
      const border_name = 'tabs-border';
      const class_show = 'active';
      const index = $(this).data('tabs');

      /** tabs header */
      $('.' + clicked_name).removeClass(class_show);
      $('.' + border_name).removeClass(class_show);
      $('.' + clicked_name + '-' + index).addClass(class_show);
      $('.' + clicked_name).children('.' + border_name + '-' + index).addClass(class_show);

      /** tabs body */
      $('.tabs-item').removeClass(class_show);
      $('.tabs-item-' + index).addClass(class_show);
      $('.tabs-body').css({ height: $('.tabs-container-item-' + index).height() + 'px' });

      if (index === 2) chartJS();
   };

   chartJS = function () {
      let arr_labels = [];
      let arr_datas = [];
      let ctx = $('#barChart');
      let arr_items = $('.tabs-container-item-2').data('graph');

      for (var key in arr_items) {
         arr_labels.push(arr_items[key].name);
         let thousands = +arr_items[key].value.replace(/,/g, '');
         arr_datas.push(thousands);
      }

      let chart_bar = new Chart(ctx, {
         type: 'bar',
         data: {
            labels: arr_labels,
            datasets: [{
               data: arr_datas,
               backgroundColor: 'rgba(0, 90, 174, 1)',
               borderColor: 'rgba(0, 90, 174, 1)',
               borderWidth: 1
            }]
         },
         options: {
            layout: {
               padding: {
                  left: 0,
                  right: 0,
                  top: 20,
                  bottom: 0
               }
            },
            responsive: true,
            legend: {
               display: false
            },
            tooltips: {
               enabled: false
            },
            scales: optionsScalesXY(),
            animation: {
               onComplete: function () {
                  let chartInstance = this.chart;
                  let ctx = this.chart.ctx;

                  ctx.textAlign = 'center';
                  ctx.fillStyle = '#000066';
                  ctx.font = '14px Frutiger-Roman';
                  ctx.strokeStyle = 'red'

                  this.data.datasets.forEach(function (dataset, id) {
                     const meta = chartInstance.controller.getDatasetMeta(id);
                     meta.data.forEach(function (bar, index) {
                        let data = dataset.data[index];
                        let data_format = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        ctx.fillText(data_format, bar._model.x, bar._model.y - 5);
                     });
                  });
               }
            }
         },
      });
   };

   const optionsScalesXY = function () {
      return {
         yAxes: [{
            gridLines: {
               display: true,
               color: 'rgba(185,211,233,.3)',
            },
            ticks: {
               display: false,
               beginAtZero: false,
               family: 'Frutiger-Bold',
               fontColor: '#000066',
               fontSize: 14
            }
         }],
         xAxes: [{
            gridLines: {
               display: false,
               color: '#fff',
            },
            ticks: {
               fontColor: '#000066',
               fontFamily: 'Frutiger-Bold',
               fontSize: 16
            }
         }]
      }
   }

   let toogleCategory = function () {
      const padding_top = 20;
      const padding_bottom = 20;
      const size_item = $('.btn-arrow').parent().parent().find('.row').height() + padding_top + padding_bottom + 'px';

      if (openRow) {
         $('.container-row-item').css({
            'height': 0,
            'padding-top': '0px',
            'padding-bottom': '0px',
            'opacity': 0
         });
         $(this).removeClass('deg-0');
         setTimeout(function () {
            $('.line-arrow').removeClass('show');
            $('.cat-col').removeClass('border-dynamic');
         }, 500);
         openRow = false;
      } else {
         $('.container-row-item').css({
            'height': size_item,
            'padding-top': padding_top + 'px',
            'padding-bottom': padding_bottom + 'px',
            'opacity': 1,
         });
         $('.line-arrow').addClass('show');
         $('.cat-col').addClass('border-dynamic');
         $(this).addClass('deg-0');
         openRow = true;
      }
   };

   $(document).ready(init);
})();